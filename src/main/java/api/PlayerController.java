package api;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@RestController
public class PlayerController {

  @Autowired
  private PlayerService playerService;

  @RequestMapping("/players")
  public List<Player> getAllPlayers() {
      return playerService.getAllPlayers();
  }

  @RequestMapping("/players/{name}")
  public Player getPlayer(@PathVariable String name) {
      return playerService.getPlayerByName(name);
  }

  @RequestMapping("/players/team/{team}")
  public List<Player> getPlayersByTeamName(@PathVariable String team) {
      return playerService.getPlayersByTeamName(team);
  }

}
