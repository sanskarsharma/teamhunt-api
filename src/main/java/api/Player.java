package api;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;


@Document(collection="players")
public class Player {

    @Id
    public String id;

    @Field("Team")
    public String team;

    @Field("Name")
    public String name;

    public String nationality;
    public Integer matches;


    public Player() {}

    public Player(String name, String team, String nationality) {
        this.name = name;
        this.team = team;
        this.nationality = nationality;
    }

    @Override
    public String toString() {
        return String.format(
          "Player [id=%s, name='%s', team='%s']",
          id, name, team
        );
    }

}
