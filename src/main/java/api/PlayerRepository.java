package api;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface PlayerRepository extends MongoRepository<Player, String> {

    Player findByName(String name);
    List<Player> findByTeam(String team);

}
